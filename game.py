from random import randint

#Prompt for user's name
user_name = input("Hi! What's your name? ")

#Computer trys to guess birth month and year (1924-2004)

for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)


#Guess 1: Caeden, were you born in guess / guess ?
    print("Guess", guess_number, ":", user_name, ", were you born in",
        month_number, "/", year_number, "?")
    response = input("Yes or No? ")
    #Promts user to answer yes or no


    #If guessed correctly, prints I knew it!
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Bye!")
    else:
        print("Drat! Lemme try again!")
